﻿using EneanceTest.Application.Entities;

namespace EneanceTest.Application.ViewModel;

public class JobViewModel
{
    public List<Job>? Jobs { get; set;} = new List<Job>();
    public List<Category>? Categories { get; set; } = new List<Category>();
}
