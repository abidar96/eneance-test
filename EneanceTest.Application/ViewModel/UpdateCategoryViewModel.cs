﻿using EneanceTest.Application.Entities;
using Microsoft.AspNetCore.Identity;

namespace EneanceTest.Application.ViewModel;

public class UpdateCategoryViewModel
{
    public string Id { get; set; }
    public string? Name { get; set; }
}
