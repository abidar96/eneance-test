﻿using EneanceTest.Application.Entities;

namespace EneanceTest.Application.ViewModel;

public class CategoryViewModel
{
    public List<Category>? Categories { get; set;}
}
