﻿namespace EneanceTest.Application.Entities;

// I named it Job because there is an ambiguity with Task of System.Threading

public class Job : Entity
{
    public string? Name { get; set; }

    public DateTime? LimiteDate { get; set; }

    public TaskState State { get; set;}
    
    public string? CategoryId { get; set; }

    public Category? Category { get; set; }
}

public enum TaskState
{
    TODO = 0,
    DONE = 1
}
