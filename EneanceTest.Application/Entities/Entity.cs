﻿namespace EneanceTest.Application.Entities;

public class Entity
{
    public string Id { get; set; }

    public bool IsTransient()
    {
        return Id == default;
    }
}
