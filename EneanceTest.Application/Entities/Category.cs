﻿using Microsoft.AspNetCore.Identity;

namespace EneanceTest.Application.Entities;

public class Category : Entity
{
    public string? Name { get; set; }

    public IEnumerable<Job>? Jobs { get; set;}

    public string? UserId { get; set; }

    public IdentityUser? User { get; set; }
}
