﻿using EneanceTest.Application.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EneanceTest.Application.Data.Configuration;

public class CategoryConfigruration : IEntityTypeConfiguration<Category>
{
    public void Configure(EntityTypeBuilder<Category> builder)
    {
        builder.HasOne(category => category.User)
       .WithMany()
       .HasForeignKey(category => category.UserId)
       .OnDelete(DeleteBehavior.Cascade);
    }
}
