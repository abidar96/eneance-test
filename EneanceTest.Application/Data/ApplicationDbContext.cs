﻿using EneanceTest.Application.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace EneanceTest.Application.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Job> Tasks  { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>()
            .Property(e => e.Id)
            .ValueGeneratedOnAdd();

            modelBuilder.Entity<Job>()
            .Property(e => e.Id)
            .ValueGeneratedOnAdd();

            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            InitializeData(modelBuilder);

            base.OnModelCreating(modelBuilder);
        }

        private void InitializeData(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>()
            .HasData(new Category()
            {
                Id="1",
                Name= "Category1",
            }, new Category()
            {
                Id = "2",
                Name = "Category2",
            });
        }

    }
}