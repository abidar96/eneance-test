﻿using EneanceTest.Application.Data;
using EneanceTest.Application.Entities;
using EneanceTest.Application.Repositories.Interfaces;

namespace EneanceTest.Application.Repositories;

public class CategoryRepository : Repository<Category>, ICategoryRepository
{
    public CategoryRepository(ApplicationDbContext dbContext) : base(dbContext)
    {
    }

    public async Task<List<Category>> GetUserCategories(string userId)
    {
        return await base.ListAsync(cat => cat.UserId == userId || cat.UserId == null);
    }
}
