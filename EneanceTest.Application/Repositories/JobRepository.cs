﻿using EneanceTest.Application.Data;
using EneanceTest.Application.Entities;
using EneanceTest.Application.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace EneanceTest.Application.Repositories;

public class JobRepository : Repository<Job>, IJobRepository
{
    public JobRepository(ApplicationDbContext dbContext) : base(dbContext)
    {
    }

    public async Task<Job> GetByIdAsyncIncludeCategory(string id)
    {
        return await DbSet.Include(x => x.Category)
            .Where(x=> x.Id == id)
            .SingleOrDefaultAsync();
    }
}
