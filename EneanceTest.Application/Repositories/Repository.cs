﻿using EneanceTest.Application.Data;
using EneanceTest.Application.Entities;
using EneanceTest.Application.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace EneanceTest.Application.Repositories;

public class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity
{
    private readonly ApplicationDbContext _dbContext;

    public Repository(ApplicationDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    protected DbSet<TEntity> DbSet => _dbContext.Set<TEntity>();


    public TEntity Add(TEntity entity)
    {
        if (entity.IsTransient())
        {
            return DbSet
                .Add(entity)
                .Entity;
        }

        return entity;
    }

    public async Task<TEntity> AddAsync(TEntity entity)
    {
        await DbSet.AddAsync(entity);

        return entity;
    }

    public async Task<int> CountAsync(System.Linq.Expressions.Expression<Func<TEntity, bool>> expr)
    {
        return await DbSet.CountAsync(expr);
    }

    public void Delete(TEntity entity)
    {
        DbSet.Remove(entity);
    }

    public Task<bool> ExistAsync(string id)
    {
        return DbSet.AnyAsync(x => x.Id == id);
    }

    public IQueryable<TEntity> GetAll()
    {
        return DbSet;
    }

    public async Task<TEntity> GetByIdAsync(string id)
    {
        return await DbSet.FindAsync(id);
    }

    public async Task<List<TEntity>> ListAsync()
    {
        return await DbSet.ToListAsync();
    }

    public async Task<List<TEntity>> ListAsync(System.Linq.Expressions.Expression<Func<TEntity, bool>> expression)
    {
        return await DbSet.Where(expression).ToListAsync();
    }

    public async Task SaveChangesAsync()
    {
        await _dbContext.SaveChangesAsync();
    }

    public void Update(TEntity entity)
    {
        _dbContext.Entry(entity).State = EntityState.Modified;
    }
}
