﻿using EneanceTest.Application.Entities;
using System.Linq.Expressions;

namespace EneanceTest.Application.Repositories.Interfaces;

public interface IRepository<TEntity> where TEntity : Entity
{
    Task<TEntity> AddAsync(TEntity entity);

    TEntity Add(TEntity entity);

    Task<int> CountAsync(Expression<Func<TEntity, bool>> expr);

    void Delete(TEntity entity);

    Task<bool> ExistAsync(string id);

    IQueryable<TEntity> GetAll();

    Task<TEntity> GetByIdAsync(string id);

    Task<List<TEntity>> ListAsync();

    Task<List<TEntity>> ListAsync(Expression<Func<TEntity, bool>> expression);

    Task SaveChangesAsync();

    void Update(TEntity entity);
}