﻿using EneanceTest.Application.Entities;

namespace EneanceTest.Application.Repositories.Interfaces;

public interface IJobRepository : IRepository<Job>
{
    Task<Job> GetByIdAsyncIncludeCategory(string id);
}
