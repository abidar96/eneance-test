﻿using EneanceTest.Application.Entities;

namespace EneanceTest.Application.Repositories.Interfaces;

public interface ICategoryRepository : IRepository<Category>
{
    Task<List<Category>> GetUserCategories(string userId);
}
