﻿using EneanceTest.Application.Entities;

namespace EneanceTest.Application.Dto;

public class JobLight
{
    public string? Name { get; set; }

    public DateTime? LimiteDate { get; set; }

    public TaskState State { get; set; }

    public string CategoryId { get; set; }
}
