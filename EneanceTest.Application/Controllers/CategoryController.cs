﻿using EneanceTest.Application.Dto;
using EneanceTest.Application.Entities;
using EneanceTest.Application.Models;
using EneanceTest.Application.Service.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Security.Claims;
using EneanceTest.Application.ViewModel;
using EneanceTest.Application.Exceptions;

namespace EneanceTest.Application.Controllers
{
    [Authorize]
    public class CategoryController : Controller
    {
        private readonly ILogger<CategoryController> _logger;
        private readonly ICategoryService _categoryService;

        public CategoryController(ILogger<CategoryController> logger, ICategoryService categoryService)
        {
            _logger = logger;
            _categoryService = categoryService;
        }

        public async Task<IActionResult> Index()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var userCategories = await _categoryService.GetUserCategoriesAsync(userId);
            var viewModel = new CategoryViewModel() { Categories = userCategories };

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Add(CategoryLight categoryLight)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var category  = new Category { Name = categoryLight.Name, UserId = userId };
            await _categoryService.CreateCategoryAsync(category);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<IActionResult> Update(string categoryID)
        {
            try
            {
                var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                var category = await _categoryService.GetCategoryByIdAndUserIdAsync(categoryID, userId);
                var viewModel = new UpdateCategoryViewModel() { Id = category.Id, Name = category.Name };
                return View(viewModel);
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel
                {
                    RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                    Message = ex.Message

                });
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost,ActionName("Update")]
        public async Task<IActionResult> UpdatePost(string categoryID, CategoryLight categoryLight)
        {
            try
            {
                var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                var category = await _categoryService.UpdateCategoryAsync(categoryID, userId, categoryLight);
                var viewModel = new UpdateCategoryViewModel() { Id = category.Id, Name = category.Name };
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel
                {
                    RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                    Message = ex.Message

                });
            }
        }

        // it could be HttpDelete and use Form or Jquery request in Front, but no much time :)
        [HttpGet, ActionName("Delete")]
        public async Task<IActionResult> Delete(string categoryID)
        {
            try
            {
                var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                await _categoryService.DeleteCategoryAsync(categoryID, userId);
                return RedirectToAction("Index");

            }
            catch (Exception ex) 
            {
                return View("Error", new ErrorViewModel
                {
                    RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                    Message = ex.Message

                });
            }

        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}