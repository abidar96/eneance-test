﻿using EneanceTest.Application.Dto;
using EneanceTest.Application.Entities;
using EneanceTest.Application.Models;
using EneanceTest.Application.Service.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Security.Claims;
using EneanceTest.Application.ViewModel;
using EneanceTest.Application.Exceptions;

namespace EneanceTest.Application.Controllers
{
    [Authorize]
    public class JobController : Controller
    {
        private readonly ILogger<CategoryController> _logger;
        private readonly ICategoryService _categoryService;
        private readonly IJobService _jobService;


        public JobController(ILogger<CategoryController> logger, ICategoryService categoryService, IJobService jobService)
        {
            _logger = logger;
            _categoryService = categoryService;
            _jobService = jobService;
        }

        public async Task<IActionResult> Index()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var userCategories = await _categoryService.GetUserCategoriesAsync(userId);
            var jobs = await _jobService.GetUserJobs(userId);


            var viewModel = new JobViewModel() 
            { 
                Categories = userCategories,
                Jobs = jobs
            };

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Add(JobLight jobLight)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            try
            {
                var job = new Job()
                {
                    Name = jobLight.Name,
                    CategoryId = jobLight.CategoryId,
                    LimiteDate = jobLight.LimiteDate,
                    State = jobLight.State
                };

                await _jobService.CreateJobAsync(userId, job);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel
                {
                    RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                    Message = ex.Message

                });
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost,ActionName("Done")]
        public async Task<IActionResult> MakeJobStateDone(string jobId)
        {
            try
            {
                var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                await _jobService.MakeItDone(jobId, userId);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel
                {
                    RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                    Message = ex.Message

                });
            }
        }

        // it could be HttpDelete and use Form or Ajax request in Front, but no much time :)
        [HttpGet, ActionName("Delete")]
        public async Task<IActionResult> Delete(string jobId)
        {
            try
            {
                var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                await _jobService.DeleteJobAsync(jobId, userId);
                return RedirectToAction("Index");

            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel
                {
                    RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                    Message = ex.Message

                });
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}