﻿using EneanceTest.Application.Dto;
using EneanceTest.Application.Entities;

namespace EneanceTest.Application.Service.Interfaces;

public interface IJobService
{
    public Task<List<Job>> GetUserJobs(string userId);
    public Task<Job> CreateJobAsync(string userId,Job job);
    public Task<Job> MakeItDone(string jobId, string userId);
    public Task DeleteJobAsync(string jobId, string userId);
}
