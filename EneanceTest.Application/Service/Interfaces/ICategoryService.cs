﻿using EneanceTest.Application.Dto;
using EneanceTest.Application.Entities;

namespace EneanceTest.Application.Service.Interfaces;

public interface ICategoryService
{
    public Task<Category> GetCategoryById(string id);
    public Task<List<Category>> GetUserCategoriesAsync(string userId);
    public Task<Category> GetCategoryByIdAndUserIdAsync(string categoryId, string UserId);
    public Task<Category> CreateCategoryAsync(Category category);
    public Task<Category> UpdateCategoryAsync(string categoryId, string userId, CategoryLight categoryLight);
    public Task DeleteCategoryAsync(string categoryId, string userId);
}
