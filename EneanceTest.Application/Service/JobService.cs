﻿using EneanceTest.Application.Dto;
using EneanceTest.Application.Entities;
using EneanceTest.Application.Exceptions;
using EneanceTest.Application.Repositories;
using EneanceTest.Application.Repositories.Interfaces;
using EneanceTest.Application.Service.Interfaces;
using System.Linq;

namespace EneanceTest.Application.Service;

public class JobService : IJobService
{
    private readonly IJobRepository _jobRepository;
    private readonly ICategoryService _categoryService;


    public JobService(IJobRepository jobRepository, ICategoryService categoryService)
    {
        _jobRepository = jobRepository;
        _categoryService = categoryService;
    }

    public async Task<Job> CreateJobAsync(string userId, Job job)
    {
        var category = await _categoryService.GetCategoryById(job.CategoryId);
        if(category == null || (category.UserId != null && category.UserId != userId))
        {
            throw new NotFoundException("No found category");
        }

        var addedJob = await _jobRepository.AddAsync(job);
        await _jobRepository.SaveChangesAsync();

        return addedJob;
    }

    public async Task DeleteJobAsync(string jobId, string userId)
    {
        var job = await _jobRepository.GetByIdAsyncIncludeCategory(jobId);
        if (job == null)
        {
            throw new NotFoundException("Cetegory not found");
        }
        if (job?.Category?.UserId != null && job?.Category?.UserId != userId)
        {
            throw new UnauthorizedException("Unauthorized to delete this entity");
        }

        _jobRepository.Delete(job);
        await _jobRepository.SaveChangesAsync();
    }

    public async Task<List<Job>> GetUserJobs(string userId)
    {
        var categoriesId = (await _categoryService.GetUserCategoriesAsync(userId))
            .Select(x=>x.Id);

        return await _jobRepository.ListAsync(x => categoriesId.Contains(x.CategoryId));
    }

    public async Task<Job> MakeItDone(string jobId, string userId)
    {
        var job = await _jobRepository.GetByIdAsyncIncludeCategory(jobId);
        if(job == null)
        {
            throw new NotFoundException("No found Job");
        }
        if (job?.Category?.UserId != null && job?.Category?.UserId != userId)
        {
            throw new UnauthorizedException("Unauthorized to update this entity");
        }

        job.State = TaskState.DONE; 
        _jobRepository.Update(job);
        await _jobRepository.SaveChangesAsync();
        
        return job;
    }
}
