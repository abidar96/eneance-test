﻿using EneanceTest.Application.Dto;
using EneanceTest.Application.Entities;
using EneanceTest.Application.Exceptions;
using EneanceTest.Application.Repositories.Interfaces;
using EneanceTest.Application.Service.Interfaces;

namespace EneanceTest.Application.Service;

public class CategoryService : ICategoryService
{
    private readonly ICategoryRepository _categoryRepository;

    public CategoryService(ICategoryRepository categoryRepository) => _categoryRepository = categoryRepository;

    public async Task<Category> CreateCategoryAsync(Category category)
    {
        var addedCat = await _categoryRepository.AddAsync(category);
        await _categoryRepository.SaveChangesAsync();
        return addedCat;
    }

    public async Task DeleteCategoryAsync(string categoryId, string userId)
    {
        var category = await _categoryRepository.GetByIdAsync(categoryId);
        if (category == null)
        {
            throw new NotFoundException("Cetegory not found");
        }
        if (category.UserId!=null && category.UserId != userId)
        {
            throw new UnauthorizedException("Unauthorized to delete this entity");
        }
        _categoryRepository.Delete(category);
        await _categoryRepository.SaveChangesAsync();
    }

    public async Task<Category> GetCategoryById(string id)
    {
        return await _categoryRepository.GetByIdAsync(id);
    }

    public async Task<Category> GetCategoryByIdAndUserIdAsync(string categoryId, string userId)
    {
        var category = await _categoryRepository.GetByIdAsync(categoryId);
        if (category == null)
        {
            throw new NotFoundException("Cetegory not found");
        }
        if (category.UserId != null && category.UserId != userId)
        {
            throw new UnauthorizedException("Unauthorized to update this entity");
        }

       return category;
    }

    public async Task<List<Category>> GetUserCategoriesAsync(string userId)
    {
        return await _categoryRepository.GetUserCategories(userId);
    }

    public async Task<Category> UpdateCategoryAsync(string categoryId, string userId, CategoryLight categoryLight)
    {
        var category = await _categoryRepository.GetByIdAsync(categoryId);
        if (category == null)
        {
            throw new NotFoundException("Cetegory not found");
        }
        if(category.UserId != null && category.UserId != userId)
        {
            throw new UnauthorizedException("Unauthorized to update this entity");
        }

        category.Name = categoryLight.Name;
        _categoryRepository.Update(category);
        await _categoryRepository.SaveChangesAsync();

        return category;
    }
}
