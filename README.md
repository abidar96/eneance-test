# Eneance Test

Ce projet est un projet Web Asp.Net avec .Net Core 7.0 basé sur une architecture monolith.

Ps: Un de mes projets en architecture propre (Hexagonale) se trouve [ici](https://gitlab.com/abidar96/bank-exalt-it)


Pour Lancer le projet, il suffit de l'importer dans Visual Studio et l'executer, sinon avec les commandes:
dotner restore
dotnet build
dotnet run

La base de données contient déja 2 catégories génerique comme demandé dans l'énoncé

Ps: aucune création de BD ou script sql est requis pour lancer l'application, Tout est automatisé.